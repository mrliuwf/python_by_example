# num = int(input("Number: "))
# result = 100 / num
# print(result)
# print('Done')
###############################
# try:
#     num = int(input("Number: "))
#     result = 100 / num
#     print(result)
#     print('Done')
# except ValueError:
#     print('无效输入')
# except ZeroDivisionError:
#     print('无效输入')
# except KeyboardInterrupt:
#     print('\nBye-bye')    # \n表示先打印回车
# except EOFError:
#     print('\nBye-bye')
###############################
# try:
#     num = int(input("Number: "))
#     result = 100 / num
#     print(result)
#     print('Done')
# except (ValueError, ZeroDivisionError):
#     print('无效输入')
# except (KeyboardInterrupt, EOFError):
#     print('\nBye-bye')
###############################
# try:
#     num = int(input("Number: "))
#     result = 100 / num
#     print(result)
#     print('Done')
# except (ValueError, ZeroDivisionError) as e:
#     print('无效输入:', e)
# except (KeyboardInterrupt, EOFError):
#     print('\nBye-bye')
###############################
# try:
#     num = int(input("Number: "))
#     result = 100 / num
# except (ValueError, ZeroDivisionError) as e:
#     print('无效输入:', e)
# except (KeyboardInterrupt, EOFError):
#     print('\nBye-bye')
#
# print(result)
# print('Done')
###############################
# try:
#     num = int(input("Number: "))
#     result = 100 / num
# except (ValueError, ZeroDivisionError) as e:
#     print('无效输入:', e)
# except (KeyboardInterrupt, EOFError):
#     print('\nBye-bye')
# else:
#     print(result)
#
# print('Done')
###############################
try:
    num = int(input("Number: "))
    result = 100 / num
except (ValueError, ZeroDivisionError) as e:
    print('无效输入:', e)
except (KeyboardInterrupt, EOFError):
    print('\nBye-bye')
else:
    print(result)
finally:
    print('Done')

