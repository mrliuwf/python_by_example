import time

def deco(func):
    def timeit():
        start = time.time()
        func()
        end = time.time()
        print(end - start)
    return timeit

@deco
def func1():
    print('%s starting...' % func1.__name__)
    time.sleep(0.5)
    print('%s end...' % func1.__name__)

@deco
def func2():
    print('%s starting...' % func2.__name__)
    time.sleep(3)
    print('%s end...' % func2.__name__)

if __name__ == '__main__':
    func1()
    func2()
