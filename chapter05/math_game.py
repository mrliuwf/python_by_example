from random import randint, choice

def add(x, y):
    return x + y

def sub(x, y):
    return x - y

def exam():
    cmds = {'+': add, '-': sub}
    nums = [randint(1, 100) for i in range(2)]
    nums.sort(reverse=True)  # 列表降序排列
    op = choice('+-')
    result = cmds[op](*nums)
    prompt = "%s %s %s = " % (nums[0], op, nums[1])
    tries = 0               # 设置计数器

    while tries < 3:
        try:
            answer = int(input(prompt))
        except:   # 简单粗暴地捕获所有异常
            continue

        if answer == result:
            print('Very good!')
            break
        print('Wrong answer.')
        tries += 1
    else:        # 此得是while的else，三次全算错才给答案
        print('%s%s' % (prompt, result))

def main():
    while True:
        exam()
        try:
            yn = input("Continue(y/n)? ").strip()[0]
        except IndexError:
            continue
        except (KeyboardInterrupt, EOFError):
            print()
            yn = 'n'    # 按下ctrl+c/ctrl+d算用户输入n

        if yn in 'nN':    # 只有n或N才结束，否则继续出题
            break

if __name__ == '__main__':
    main()
