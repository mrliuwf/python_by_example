import random

all_chs = 'abcdefghijklmnopqrstuvwxyz\
ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

result = ''
for i in range(8):
    ch = random.choice(all_chs)
    result += ch

print(result)
