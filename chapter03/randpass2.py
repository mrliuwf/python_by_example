import random
import string

all_chs = string.ascii_letters + string.digits
result = ''

for i in range(8):
    ch = random.choice(all_chs)
    result += ch

print(result)
