import socket
from time import strftime

def udp_server(host, port):
    addr = (host, port)
    s = socket.socket(type=socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(addr)

    while True:
        try:
            data, cli_addr = s.recvfrom(1024)
        except KeyboardInterrupt:
            break

        data = '[%s] %s' % (strftime('%H:%M:%S'), data.decode())
        s.sendto(data.encode(), cli_addr)

    s.close()

if __name__ == '__main__':
    host = ''
    port = 12345
    udp_server(host, port)
